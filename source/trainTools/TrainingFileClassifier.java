package trainTools;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class TrainingFileClassifier {
	
	
	/**
	 * 
	 * @param pathCSV Ruta al archivo CSV con las reviews de entrenamiento
	 * @param pathDestino Debe ser un nombre de carpeta EXISTENTE donde quedar�n las carpetas que contengan los archivos clasificados
	 * @throws IOException
	 */
	public TrainingFileClassifier(String pathCSV, String pathDestino) throws IOException {
		File reviews = new File(pathCSV);
		
		Scanner sc = new Scanner(reviews);
		
		while(sc.hasNextLine())
		{
			String reviewLine = sc.nextLine();
			String[] review_classif = reviewLine.split(";");
			if(review_classif[1].equals("1"))
			{
				File nuevoTFReview = new File(pathDestino+"/Negative", ""+"tr"+Math.random()*4+".txt");
				if(!nuevoTFReview.exists())
					nuevoTFReview.createNewFile();
				FileWriter fw = new FileWriter(nuevoTFReview);
				PrintWriter pw = new PrintWriter(fw);
				pw.write(review_classif[0]);
				pw.close();
				fw.close();
			}else if(review_classif[1].equals("2"))
			{
				File nuevoTFReview = new File(pathDestino+"/Neutral", ""+"tr"+Math.random()*4+".txt");
				if(!nuevoTFReview.exists())
					nuevoTFReview.createNewFile();
				FileWriter fw = new FileWriter(nuevoTFReview);
				PrintWriter pw = new PrintWriter(fw);
				pw.write(review_classif[0]);
				pw.close();
				fw.close();			
			}else if(review_classif[1].equals("3"))
			{
				File nuevoTFReview = new File(pathDestino+"/Positive", ""+"tr"+Math.random()*4+".txt");
				if(!nuevoTFReview.exists())
					nuevoTFReview.createNewFile();
				FileWriter fw = new FileWriter(nuevoTFReview);
				PrintWriter pw = new PrintWriter(fw);
				pw.write(review_classif[0]);
				pw.close();
				fw.close();				
			}else if (review_classif[1].equals("0"))
			{
				// cero significa ignorar	
			}else {
				System.err.println("QUIEN FUE EL MICO "+reviewLine);
			}
			
		} 
		
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			TrainingFileClassifier tfc = new TrainingFileClassifier("C:/Users/Danilo/Dropbox/From Big Data to Content Analysis/Taller 2/csv dummy0510.csv", "C:/Users/Danilo/Dropbox/From Big Data to Content Analysis/Taller 2/exampleOutputPath");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
