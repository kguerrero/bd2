package mundo;

import java.net.URL;
import java.net.URLConnection;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import trainTools.EscribirCSV;
public class AnalisisHtml 
{
	
	public final static Pattern F_LLAVE_PLACE = Pattern.compile("<p>\\s*(.+)\\s*</p>", Pattern.MULTILINE);
		
	public String analizarArticulo(String url) {
		String htmlRta = getHtml(url);
		String[] split1 = htmlRta.split("<article>");
		String[] split2 = split1[1].split("</article>");
		return obtenerArticle(split2[0]);
		
	}
	
    private String getHtml( String url )
    {
        String content = null;
        URLConnection connection = null;
        try {
          connection =  new URL(url).openConnection();
          connection.addRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1)");
          Scanner scanner = new Scanner(connection.getInputStream());
          scanner.useDelimiter("\\Z");
          content = scanner.next();
        }catch ( Exception ex ) {
            ex.printStackTrace();
        }
        return content;
    }
    
    public String obtenerArticle( String htmlCodigo )
    {
    	
        Matcher codigo = F_LLAVE_PLACE.matcher(htmlCodigo);
        String respuestas = "";
        EscribirCSV escribir = new EscribirCSV();
        while( codigo.find() )
        {
                String rta= codigo.group(1);
                escribir.escribir(rta);
                respuestas+=rta;
        }
        escribir.cerrar();
        System.out.println("CERRO");
        return respuestas;
    }
}
