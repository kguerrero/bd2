package mundo;

import java.io.IOException;
import java.util.HashMap;
import java.util.Set;

public class Sentimentator {


	private PolarityBasic evaluator;

	public Sentimentator(HashMap<String, String> feed)
	{
		try {
			evaluator =new PolarityBasic(feed);
			evaluator.trainAtomic();			

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public Sentimentator()
	{
		try {
			evaluator =new PolarityBasic();
			evaluator.trainAtomic();			

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	public String evaluateAtomic(String review) throws ClassNotFoundException, IOException
	{
		if(evaluator==null)
			throw new ClassNotFoundException("Debe inicizar el objeto clasificador");
		else
			return evaluator.evaluateAtomic(review);
	}

}
