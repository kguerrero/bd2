<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" import="java.util.*" pageEncoding="ISO-8859-1"%>
<%@ page import="mundo.ReadTest"%>
<%
	Object[] analysisResult = ReadTest.getFeed();
	request.setAttribute("results", analysisResult[0]);
	request.setAttribute("resumen", analysisResult[1]);
	request.setAttribute("counters", analysisResult[2]);
	String path = request.getContextPath();
	String pageURL = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><%=pageURL%></title>
<link rel="stylesheet" href="css/style.css" type="text/css"
	media="screen" />

<script type="text/javascript" src="js/accordian.pack.js"></script>
</head>
<body onload="new Accordian('accordian',3,'header_highlight');">
	<div id="header">
		<h1>
			<a href="#"><%=path%></a>
		</h1>
		<h2><%=new Date().toString()%></h2>
	</div>

	<div id="accordian">
		<!--Parent of the Accordion-->


		<!--Start of each accordion item-->
		<div id="test-header" class="accordion_headings header_highlight">Welcome</div>
		<!--Heading of the accordion ( clicked to show n hide ) -->

		<!--Prefix of heading (the DIV above this) and content (the DIV below this) to be same... eg. foo-header & foo-content-->

		<div id="test-content">
			<!--DIV which show/hide on click of header-->

			<!--This DIV is for inline styling like padding...-->
			<div class="accordion_child">
				<img src="images/img_example.jpg" alt="image example" />
				<p>The reviews have been analyzed.</p>

				<h2>Given text</h2>
					<c:forEach var="country" items="${resumen}">
						<h3>${country.key}</h3>
						<div>
							<blockquote>

								<em>${country.value}</em>

							</blockquote>
						</div>
					</c:forEach>

			</div>

		</div>
		<!--End of each accordion item-->


		<!--Start of each accordion item-->
		<div id="test1-header" class="accordion_headings">Result</div>
		<!--Heading of the accordion ( clicked to show n hide ) -->

		<!--Prefix of heading (the DIV above this) and content (the DIV below this) to be same... eg. foo-header & foo-content-->

		<div id="test1-content">
			<!--DIV which show/hide on click of header-->

			<!--This DIV is for inline styling like padding...-->
			<div class="accordion_child">

				<h2>List Example</h2>
				<ul>
					<c:forEach var="country" items="${counters}">
						<li>${country.key}: ${country.value}</li>
					</c:forEach>
				</ul>

				<h2>Analysis Result</h2>
				<table>
					<tbody>
						<tr>
							<th>Tittle</th>
							<th>Sentiment</th>
						</tr>
						<c:forEach var="country" items="${results}">
							<tr>
								<td>${country.key}</td>
								<td>${country.value.get("sentiment")}</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>

		</div>
		<!--End of each accordion item-->

		<!--Start of each accordion item-->
		<div id="test3-header" class="accordion_headings">Links</div>
		<!--Heading of the accordion ( clicked to show n hide ) -->

		<!--Prefix of heading (the DIV above this) and content (the DIV below this) to be same... eg. foo-header & foo-content-->

		<div id="test3-content">
			<!--DIV which show/hide on click of header-->

			<!--This DIV is for inline styling like padding...-->
			<div class="accordion_child">

				<p>Some links for your viewing pleasure:</p>
				<ul class="links">
					<c:forEach var="country" items="${results}">
						<li><a href="${country.value.get('link')}">${country.key}</a></li>
					</c:forEach>
				</ul>
			</div>

		</div>
		<!--End of each accordion item-->







	</div>
	<!--End of accordion parent-->



	<div id="footer">
		<p class="validate">
			<a href="http://validator.w3.org/check?uri=referer">XHTML</a> | <a
				href="http://jigsaw.w3.org/css-validator/">CSS</a><br /> <a
				href="#content">Top</a>
		</p>


		<!-- Please leave this line intact -->
		<p>
			Design: <a href="http://www.sixshootermedia.com">Six Shooter
				Media</a>. <br /> AJAX: <a
				href="http://www.dezinerfolio.com/2007/07/19/simple-javascript-accordions/">Simple
				Accordions</a>. Iconography: <a
				href="http://www.famfamfam.com/lab/icons/silk/">FamFamFam</a>.
			Sponsor: <a href="http://www.yours4money.com" rel="nofollow">Casino</a><br />
			<!-- you can delete below here -->
			� All your copyright BD.
		</p>
	</div>

</body>
</html>
